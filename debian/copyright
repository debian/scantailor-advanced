Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ScanTailor Advanced
Upstream-Contact: 4lex4 <4lex49@zoho.com>
Source: https://github.com/4lex4/scantailor-advanced

Files: *
Copyright: 2007-2016 Joseph Artsimovich <joseph.artsimovich@gmail.com>
           2011 Petr Kovar <pejuko@gmail.com>
           2016-2019 4lex4 <4lex49@zoho.com>
License: GPL-3+

Files: debian/*
Copyright: 2013-2015 Daniel Stender <debian@danielstender.com> 
           2009 Artem Popov <artfwo@gmail.com>
           2010 Alessio Treglia <quadrispro@ubuntu.com>
           2016-2017 Jason Crain <jason@inspiresomeone.us>
           2019 Andreas Noteng <andreas@noteng.no>
           2019 nicoo <nicoo@debian.org>
License: GPL-3+

Files: src/imageproc/GaussBlur.cpp
Copyright: 2007-2013 Joseph Artsimovich <joseph.artsimovich@gmail.com>
           1995 Spencer Kimball, Peter Mattis
License: GPL-3+
Comment: Based on code from the GIMP project.

Files: src/resources/icons/aqua-sphere*
       src/resources/icons/left_page*
       src/resources/icons/right_page*
       src/resources/icons/single_page*
       src/resources/icons/two_pages*
Copyright: ScanTailor developers or contributors
License: GPL-3+

Files: src/resources/icons/stock-*
Copyright: The GNU Image Manipulation Project (GIMP), http://gimp.org/
License: GPL-2+

Files: src/resources/icons/object-*
Copyright: F-Spot, http://f-spot.org/
License: GPL-2+

Files: src/resources/icons/play-*
       src/resources/icons/stop-*
Copyright: Amarok (derived from default-theme-clean.svg),  http://amarok.kde.org/
License: GPL-2+

Files: src/resources/icons/big-*
       src/resources/icons/insert-*
Copyright: not-applicable 
License: public-domain
 The icons in this repository are herefore released into the Public Domain.
Comment: Origin of files: http://tango.freedesktop.org/

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License
 can be found in the `/usr/share/common-licenses/GPL-3' file.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems the full text of the GNU General Public License
 can be found in the `/usr/share/common-licenses/GPL-2' file.
